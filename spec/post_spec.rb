
describe "cadastro" do
    it "novo usuario" do
        result = HTTParty.post(
            "http://dummy.restapiexample.com/api/v1/create", 
            body: { "id":"1", "employee_name":"Tiger Nixon",
             "employee_salary":"320800","employee_age":"61" }.to_json,
             headers: {
                "Content-Type" => "application/json",
            },
        )
        expect(result.response.code).to eql "200"
    end         
end